# WorkOrders

Display all work orders and worker details from two APIS which can be filtered worker name, also the ability to toggle by latest and earliest orders. 

![UI](https://bitbucket.org/shanilplusplus/hatchw-ts/raw/fbb502fb6f293d9270aafeba42653768385badcf/public/hatchw.png)  
![API1](https://bitbucket.org/shanilplusplus/hatchw-ts/raw/fbb502fb6f293d9270aafeba42653768385badcf/public/api1.png)  
![API2](https://bitbucket.org/shanilplusplus/hatchw-ts/raw/fbb502fb6f293d9270aafeba42653768385badcf/public/api2.png)  



### Prerequisites

you need to have node installed in your computer. 


## Getting Started

to get started on you local machine, clone this repo, and install all the node modules.



### Installing
cd into the folder
install npm dependencies


### Running Locally

```
npm install
npm start
```

### View Demo
[workOrders](https://hatchw-ts.vercel.app/)

## Built With

* [React.js](https://reactjs.org/) - for React and ReactDom libraries
* [babel](https://rometools.github.io/rome/) - compiling and transpling
* [sass](https://rometools.github.io/rome/) - for scss


## Authors

* **Shaun Sigera** (http://sigera.ca/)


## License

This project is licensed under the MIT License

