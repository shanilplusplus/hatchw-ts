import React,{useState, useEffect}  from 'react';
import moment from 'moment';



function App() {

  const [workOrders, setworkOrders] = useState([]);
  const [users, setUsers] = useState([]);
  const [searchinput, setSearchInput] = useState('');
  const [checkedInput, setCheckedInput] = useState(false)


  useEffect(() => {
    fetchAllWorkOrders();
  }, [])

  useEffect(() => {
    fetchWorkerDetailsAPI();
  }, [workOrders])



  const fetchAllWorkOrders = () => {
    fetch(process.env.REACT_APP_ALL_ORDERS)
    .then(response => response.json())
    .then(({orders}) => {

      setworkOrders(orders)

    })
  }

  const fetchWorkerDetailsAPI = () => {
      if(workOrders){
        workOrders.map(e => {
          fetch(`${process.env.REACT_APP_WORKER_DETAILS}${e.workerId}`)
          .then(response => response.json())
          .then(({worker}) => {
  
            if(e.workerId === worker.id){
              let flattenedArray =  Object.assign(e, {
              workerCompanyName: worker.companyName,
              workerName       : worker.name,
              workerImage      : worker.image,
              wID              : worker.id,
              workerEmail      : worker.email
            })
  
              setUsers(prevState => [...prevState, flattenedArray])
            }
          });
        })
      }
  }

  console.log(users, "users")

  return (
    <div className="workorders__container">
      <h2 className="workorders__heading">All Work Orders</h2>

      <div className="workorders__searchByInput">
        <input 
        type="text" 
        name="search" 
        className="workorders__searchbox" 
        onChange={(e) => setSearchInput(e.target.value.toLowerCase())}  
        placeholder="Filter by worker name.."/>
      </div>

      <div className="workorders__checkbox">
        <p className="workorders__checkbox--title-left">Earliest First</p>
          <input 
          type="checkbox" 
          className="workorders__checkbox-input" 
          onChange={(e) => {setCheckedInput(e.target.checked)}} />
        <p className="workorders__checkbox--title-right">Latest First</p>
      </div>


      <div className="workorders__allorders">
            {users && users.sort((a,b) => {
              if(checkedInput){
                return  b.deadline - a.deadline
              } else{
                return  a.deadline - b.deadline
              }
            }).filter((order) => {
                if(searchinput === ""){
                  return order;
                } else if(order.workerName.toLowerCase().includes(searchinput)){
                  return order;
                }
            }).map(order => {
              if(order){
                return(
                  <div className="workorders__order-details" key={order.deadline}>
                    <p className="workorders__order-details-name">{order.name}</p>
                    <p className="workorders__order-details-description">{order.description}</p>

                    <div className="workorders__worker">
                      <div className="workorders__worker--img">
                        <img src={`${order.workerImage}`} alt="" />
                      </div>
                      <div className="workorders__worker--description">
                          <p>{order.workerName}</p>
                          <p>{order.workerCompanyName}</p>
                          <p>{order.workerEmail}</p>
                      </div>
                    </div>     
                    <p className="workorders__order-deadline">{moment(order.deadline).format('MMMM Do YYYY, h:mm:ss a')}</p>
                    
                  </div>
                )


              }

              return  null;
              
            })}

      </div>


    </div> 
  );
}

export default App;
